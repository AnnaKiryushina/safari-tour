(function($){
			$(window).on("load",function(){
				
				$(".offer-items.content").mCustomScrollbar({
					theme: "dark",
					setHeight: "340px"
				});

				$(".tourism-items.content").mCustomScrollbar({
					theme: "dark",
					setHeight: "260px"
				});	

				$('.mCSB_dragger_bar').css({
					'background': 'url("data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAmBAMAAADgsHz9AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAElBMVEWUkpJPT09OTk7b0dHc0dEAAAD/vGvdAAAAAWJLR0QF+G/pxwAAAAlwSFlzAAALEgAACxIB0t1+/AAAAAd0SU1FB+IBEwwaHTKlro4AAAAbSURBVBjTY2AgGjAqAQEDA7MLEJAhSCGgue0A+ngJqesnXhYAAAAASUVORK5CYII=")',
					'height': '38px',
					'width': '11px',
					'max-height': '38px'
				});

				$('.mCSB_draggerRail').css({
					'background': '#949292'
				});

				$('.mCSB_dragger').css({
					'min-height': '38px',
					'height': '200px'
				});

				$('#mCSB_1_dragger_vertical .mCSB_dragger').css({
					'min-height': '38px',
					'height': '200px'
				});


				$(".disable-destroy a").click(function(e){
					e.preventDefault();
					var $this=$(this),
						rel=$this.attr("rel"),
						el=$(".content"),
						output=$("#info > p code");
					switch(rel){
						case "toggle-disable":
						case "toggle-disable-no-reset":
							if(el.hasClass("mCS_disabled")){
								el.mCustomScrollbar("update");
								output.text("$(\".content\").mCustomScrollbar(\"update\");");
							}else{
								var reset=rel==="toggle-disable-no-reset" ? false : true;
								el.mCustomScrollbar("disable",reset);
								if(reset){
									output.text("$(\".content\").mCustomScrollbar(\"disable\",true);");
								}else{
									output.text("$(\".content\").mCustomScrollbar(\"disable\");");
								}
							}
							break;
						case "toggle-destroy":
							if(el.hasClass("mCS_destroyed")){
								el.mCustomScrollbar();
								output.text("$(\".content\").mCustomScrollbar();");
							}else{
								el.mCustomScrollbar("destroy");
								output.text("$(\".content\").mCustomScrollbar(\"destroy\");");
							}
							break;
					}
				});
				
			});
		})(jQuery);