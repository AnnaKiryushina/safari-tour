function Point(x, y){
    this.x = x || 0;
    this.y = y || 0;
};

function distance(pt1, pt2){
    var x = pt1.x - pt2.x;
    var y = pt1.y - pt2.y;
    return Math.sqrt(x * x + y * y);
};

function continentFocusOn(continent)
{
    var con = $('[aria-label="' + continent + '"]');
    $(con).hover(function(){
        for(var i = 0; i < con.length; i++)
        {
            $(con[i]).find('path').attr('fill', '#62b1ce');
        }    
    }, function(){
        for(var i = 0; i < con.length; i++)
        {
            $(con[i]).find('path').attr('fill', '#85c1d8');
        }
    });
}

function continentCursorOut(continent)
{
    var con = $('[aria-label="' + continent + '"]');
    $('con').mouseleave(function (e){ // событие выхода курсора с континета
        alert("leave");
        for(var i = 0; i < con.length; i++)
        {
            $(con[i]).find('path').attr('fill', 'blue');
        } 
    });
}


function continentClick(continent, tooltip) {
    $(continent).click(function (e) {
        var tooltipW = $('.tourism-tooltip').width() - 80;//$('.tourism-list').width();
        var tooltipH = $('.tourism-tooltip').height(); //$('.tourism-list').height();

        var windowW = $(window).width();
        var windowH = $(window).height();

        var clickX = e.offsetX;
        var clickY = e.offsetY;

        var mapGlobalX = $('.tourist-map__content').offset().left;
        var mapGlobalY1 = $('.tourist-map__content').offset().top;
        var mapH = $('.tourist-map__content').height();
        var mapGlobalY2 = mapGlobalY1 + mapH;

        var innerX, innreY;

        //Tooltip x-coordinate
        if(windowW - clickX < tooltipW) innerX = clickX - tooltipW - 100;
        else innerX = clickX;

        //Tooltip y-coordinate
        if(mapH - clickY < tooltipH) innreY = mapH - tooltipH;
        else if(clickY < tooltipH) innreY = clickY;
        else innreY = clickY;

        //alert(mapH + ', ' + clickY);
        //alert(innreY - 338);
        //Drawing a tooltip's triangle

        var svgPos, p1, p2, p3;
        if(windowW - clickX < tooltipW){}
        else {
            if (clickY < tooltipH / 2) {
                $('.tooltip-arrow').css({'top': '0'});
                p1 = new Point(0, clickY);
                p2 = new Point(80, 30);
                p3 = new Point(80, 50);
            }
            if (clickY > tooltipH / 2 && clickY < tooltipH) {
                $('.tooltip-arrow').css({'bottom': '0'});
                p1 = new Point(0, clickY);
                p2 = new Point(80, tooltipH);
                p3 = new Point(80, tooltipH);
            }
            if (mapH - clickY < tooltipH / 2) {
                $('.tooltip-arrow').css({'bottom': '0'});
                p1 = new Point(0, tooltipH - clickY);
                p2 = new Point(80, 10);
                p3 = new Point(80, 30);
            }
            if (mapH - clickY > tooltipH / 2 && mapH - clickY < tooltipH) {
                $('.tooltip-arrow').css({'top': '0'});
                p1 = new Point(0, mapH - clickY);
                p2 = new Point(80, 30);
                p3 = new Point(80, 50);
            }
        }
        //var pointsStr = p1.y + ',' + p1.y + ' ' + p2.x + ',' + p2.y + ' ' + p3.x + ',' + p3.y;
        //var t = $('<svg class="tooltip-arrow" width="80" height="' + tooltipH/2 + '"><polygon class="arrowInner" points="' + pointsStr + '" fill="#e5e1e1" filter="url(-5px -5px 5px #000)" stroke="white" stroke-width="1"/></svg>').appendTo(".tourism-tooltip");
        //pointsStr = '';

        //--

        var pos = $(this).offset();
        var elem_left = pos.left;
        var elem_top = pos.top;
        //alert(e.offsetY + $('.tourism-list').height() + ", " + $(window).height());
        var Xinner = e.offsetX; //e.pageX - elem_left;
        var Yinner = e.offsetY + $('.tourism-list').height() > elem_top + $('.tourism-list').height() ? elem_top + $('.tourism-list').height() - $('.tourism-list').height() : e.offsetY; //e.pageY - elem_top;
        $(tooltip).css({
            'visibility': 'visible',
            'top': innreY,
            'left': innerX
        });
    });
}

function continentFocusOut(continent, tooltip) {
    $('.tourist-map__content').mouseup(function (e){ // событие клика по карте
        if (!$(tooltip).is(e.target) && $(tooltip).has(e.target).length === 0 && !continent.is(e.target) && continent.has(e.target).length === 0) {
            // если клик был не по нашему блоку и не по его дочерним элементам
            $(tooltip).css('visibility', 'hidden'); // скрываем его
            $('.tooltip-arrow').remove();
        }
    });
}
$(document).ready(function () {
    (function() {

        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });

    })();

    new WOW().init();


    //Sale slider
    $(".sale-slider").slick({
        centerMode: true,
        autoplay: true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 3000
    });

    $('.slick-prev').html('');
    $('.slick-next').html('');


    //--

    //Select Packeges

    var packegesList = $('.package-select');
    var opts = ["South America Packeges", "North America Packeges", "Europe Packeges", "Asia Packeges", "Africa Packeges", "Australia Packeges"];
    var optsLen = opts.length;
    var strOpt = '';

    for(var i = 0; i < optsLen; i++)
    {
        strOpt = "<option class='opt' value='" + i + "'>" + opts[i] + "</option>"; 
        $(packegesList).append(strOpt);
    }

    $('.opt').css({
        'height': '1em'
    });

    //--

    var dots = $('.map-dots').find('g');
    var northAmerica = $('[aria-label="North America "]');
    var southAmerica = $('[aria-label="South America "]');
    var asia = $('[aria-label="Asia "]');
    var australia = $('[aria-label="Australia and Oceania "]');
    var europe = $('[aria-label="Europe "]');
    var africa = $('[aria-label="Africa "]');

    //var toolipArrow = $('<div class="arrow"></div></div>').appendTo(".tourism-list");
    //var toolipArrow = $('<svg class="tooltip-arrow" width="230" height="140"><polygon points="5,135 115,5 225,135" fill="violet" stroke="purple" stroke-width="5"/></svg>').appendTo(".tourism-list");

    continentFocusOn("North America ");
    continentFocusOn("South America ");
    continentFocusOn("Asia ");
    continentFocusOn("Australia and Oceania ");
    continentFocusOn("Europe ");
    continentFocusOn("Africa ");

    continentClick(northAmerica, '.north-america');
    continentClick(southAmerica, '.south-america');
    continentClick(asia, '.asia');
    continentClick(australia, '.australia');
    continentClick(europe, '.europe');
    continentClick(africa, '.africa');

    continentFocusOut(northAmerica, '.north-america');
    continentFocusOut(southAmerica, '.south-america');
    continentFocusOut(asia, '.asia');
    continentFocusOut(australia, '.australia');
    continentFocusOut(europe, '.europe');
    continentFocusOut(africa, '.africa');

});
